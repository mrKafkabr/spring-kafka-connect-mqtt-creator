package springkafkaConnect.mqttcreator.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import springkafkaConnect.mqttcreator.mqttConnectConfig.MqttConnectConfig;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Connect {

    @JsonProperty("name")
    private String name;

    @JsonProperty("config")
    private MqttConnectConfig config;

    //SETTERS
    public void setName(String name) {
        this.name = name;
    }

    public void setConfig(MqttConnectConfig config) {
        this.config = config;
    }
}
