package springkafkaConnect.mqttcreator.model;

public class Parametros {

    private String connectName;

    private String kafkaTopic;

    private String mqttTopic;

    //GETTERS
    public String getConnectName() {
        return connectName;
    }

    public String getKafkaTopic() {
        return kafkaTopic;
    }

    public String getMqttTopic() {
        return mqttTopic;
    }

}
