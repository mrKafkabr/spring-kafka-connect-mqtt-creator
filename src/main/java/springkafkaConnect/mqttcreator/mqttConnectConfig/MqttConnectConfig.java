package springkafkaConnect.mqttcreator.mqttConnectConfig;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MqttConnectConfig {

    @JsonProperty(value = "connector.class")
    private String connectorClass = "io.confluent.connect.mqtt.MqttSourceConnector";

    @JsonProperty(value = "mqtt.qos")
    private String mqttQos = "1";

    @JsonProperty(value = "tasks.max")
    private String taskMax = "2";

    @JsonProperty(value = "mqtt.clean.session.enabled")
    private String mqttCleanSessionEnabled = "true";

    @JsonProperty(value = "mqtt.server.uri")
    private String mqttServerUri;

    @JsonProperty(value = "mqtt.connect.timeout.seconds")
    private String mqttConnectTimeoutSeconds = "30";

    @JsonProperty(value = "key.converter.schemas.enable")
    private String keyConverterSchemasEnable = "false";

    @JsonProperty(value = "value.converter.schemas.enable")
    private String valueConverterSchemasEnable = "false";

    @JsonProperty(value = "mqtt.topics")
    private String mqttTopics;

    @JsonProperty(value = "mqtt.keepalive.interval.seconds")
    private String mqttKeepaliveIntervalSeconds = "60";

    @JsonProperty(value = "kafka.topic")
    private String kafkaTopic;

    @JsonProperty(value = "value.converter")
    private String valueConverter = "org.apache.kafka.connect.converters.ByteArrayConverter";

    @JsonProperty(value = "key.converter")
    private String keyConverter = "org.apache.kafka.connect.json.JsonConverter";

    @JsonProperty(value = "transforms")
    private String transforms = "createMap,createKey,extractInt";

    @JsonProperty(value = "transforms.createMap.type")
    private String transformsCreateMapType = "org.apache.kafka.connect.transforms.HoistField$Value";

    @JsonProperty(value = "transforms.createMap.field")
    private String transformsCreateMapField = "id";

    @JsonProperty(value = "transforms.createKey.type")
    private String transformsCreateKeyType = "org.apache.kafka.connect.transforms.ValueToKey";

    @JsonProperty(value = "transforms.createKey.fields")
    private String transformsCreateKeyFields = "id";

    @JsonProperty(value = "transforms.extractInt.type")
    private String transformsExtractIntType = "org.apache.kafka.connect.transforms.ExtractField$Value";

    @JsonProperty(value = "transforms.extractInt.field")
    private String transformsExtractIntField = "id";

    public void setMqttServerUri(String mqttServerUri) {
        this.mqttServerUri = mqttServerUri;
    }

    public void setMqttTopics(String mqttTopics) {
        this.mqttTopics = mqttTopics;
    }

    public void setKafkaTopic(String kafkaTopic) {
        this.kafkaTopic = kafkaTopic;
    }
}
