package springkafkaConnect.mqttcreator.retrofit;

import org.springframework.beans.factory.annotation.Value;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import springkafkaConnect.mqttcreator.model.Connect;


import java.io.IOException;

public class ApiClient {

    private String URI;

    public ApiClient(@Value("${kafka.connect.uri}") String uri) {
    this.URI = uri;
    }

    public Connect createMqttConnect(Connect connect) throws IOException {

        Retrofit retrofit = getRetrofit();

        ApiService apiService = retrofit.create(ApiService.class);
        Call<Connect> connect1 = apiService.createConnect(connect);
        return connect1.execute().body();
    }

    public void deleteMqttConnect(String name) throws Exception {
        Retrofit retrofit = getRetrofit();
        ApiService apiService = retrofit.create(ApiService.class);
        try{
            Call<Void> responseBodyCall = apiService.deleteConnect(name);
            responseBodyCall.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    System.out.println(">>>> " + response);
                }

                @Override
                public void onFailure(Call<Void> call, Throwable throwable) {
                    System.out.println("<<<<< Comunicação falhou");
                }
            });

        }catch (Exception e){
            System.out.println(e.getMessage());
            throw new Exception(e);
        }
    }

    private Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(URI)
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
    }
}
