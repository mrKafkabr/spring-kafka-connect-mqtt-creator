package springkafkaConnect.mqttcreator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MqttCreatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(MqttCreatorApplication.class, args);
	}

}
