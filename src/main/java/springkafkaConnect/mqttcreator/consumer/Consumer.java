package springkafkaConnect.mqttcreator.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import springkafkaConnect.mqttcreator.model.Connect;
import springkafkaConnect.mqttcreator.model.Parametros;
import springkafkaConnect.mqttcreator.mqttConnectConfig.MqttConnectConfig;
import springkafkaConnect.mqttcreator.retrofit.ApiClient;
import java.io.IOException;

@Component
public class Consumer {

    private final static String TOPIC_CREATE_CONNECT = "createMqttConnect";
    private final static String TOPIC_DELETE_CONNECT = "deleteMqttConnect";

    private final String uri;

    @Autowired
    public Consumer(@Value("${kafka.connect.uri}") String uri) {
        this.uri = uri;
    }

    @KafkaListener(topics = {TOPIC_CREATE_CONNECT})
    public void createConnect(@Payload String message) throws IOException {
        Parametros parametros = jsonToParametros(message);
        createMqttConnect(parametros);
    }

    @KafkaListener(topics = {TOPIC_DELETE_CONNECT})
    public void deleteMqttConnect(@Payload String message) {
        try {
            callDeleteConnectApi(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Parametros jsonToParametros(String json) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Parametros.class);
    }

    private void createMqttConnect(Parametros parametros) {
        Connect connect = createConnect(parametros.getConnectName(), parametros.getKafkaTopic(), parametros.getMqttTopic());
        try {
            callCreateConnectApi(connect);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void callDeleteConnectApi(String connectName) throws Exception {
        ApiClient apiClient = new ApiClient(uri);
        apiClient.deleteMqttConnect(connectName);
    }

    private void callCreateConnectApi(Connect connect) throws IOException {
        ApiClient apiClient = new ApiClient(uri);
        apiClient.createMqttConnect(connect);
    }

    private static Connect createConnect(String connectName, String kafkaTopic, String mqttTopic) {
        Connect connect = new Connect();
        connect.setName(connectName);
        connect.setConfig(configMqttConnect(kafkaTopic, mqttTopic));
        return connect;
    }

    private static MqttConnectConfig configMqttConnect(String kafkaTopic, String mqttTopic) {
        MqttConnectConfig mqttConnectConfig = new MqttConnectConfig();
        mqttConnectConfig.setKafkaTopic(kafkaTopic);
        mqttConnectConfig.setMqttTopics(mqttTopic);
        mqttConnectConfig.setMqttServerUri("tcp://mosquitto-server:1883");
        return mqttConnectConfig;
    }
}
